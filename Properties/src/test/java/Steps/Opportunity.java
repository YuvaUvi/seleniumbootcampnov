package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class Opportunity extends BaseClass{
	
	@Given ("Click on Sales")
	public void clickonSales() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Sales']")).click();
	Thread.sleep(10000);
	}
	
	@And ("Click on Opportunity")
	public void ClickonOpportunity() throws InterruptedException {
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("(//span[text()='Opportunities'])[1]")));
	Thread.sleep(10000);
	}
	
	
	@And ("Click on New")
	public void ClickOnNew () throws InterruptedException {
	driver.findElement(By.xpath("//div[text() = 'New']")).click();
	Thread.sleep(10000);
	}
	
	@And("EnterSalesforceName  {string}")
	public void enter_salesforce_name(String EnterSalesforce) throws InterruptedException {
	driver.findElement(By.xpath("//input[@name = 'Name']")).sendKeys(EnterSalesforce);
	Thread.sleep(10000);
	
	}

	
	@Then ("EnterDate {string}")
	public void EnterDate (String EnterDate) throws InterruptedException {
	driver.findElement(By.xpath("//input[@name = 'CloseDate']")).sendKeys(EnterDate);
	Thread.sleep(10000);
	
	}
	
	@Then ("clickdropdown")
	public void clickDropdown () throws InterruptedException {
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("(//div[@role='none']//input)[3]")));
	Thread.sleep(10000);
	}
	
	@And ("clickAnalysis")
	public void clickAnalaysis () throws InterruptedException {
	driver.findElement(By.xpath("//div[@role = 'listbox']/lightning-base-combobox-item[4]/span[2]")).click();
	Thread.sleep(1000);
	}
	@And ("clickSave")
	public void Save () {
	driver.findElement(By.xpath("//li[@class ='slds-button-group-item visible']//button[text() = 'Save']")).click();

    }
	@Given ("Click Sales")
	public void clickSales() throws InterruptedException {
	driver.findElement(By.xpath("//p[text()='Sales']")).click();
	Thread.sleep(10000); 
	}
	
	@And ("ClickOpportunity")
	public void clickOpprotunity () throws InterruptedException {
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("(//span[text()='Opportunities'])[1]")));
	Thread.sleep(10000);
	
	}
	
	@And ("Search by Name {string}")
	public void SearchByName (String SearchName) throws InterruptedException {
	WebElement search = driver.findElement(By.xpath("//input[@placeholder='Search this list...']"));
	search.sendKeys(SearchName);
	search.sendKeys(Keys.ENTER);
	Thread.sleep(4000);
	
	}
	
	@And ("Click DropDown")
	public void clickdropdown () throws InterruptedException {
	driver.findElement(By.xpath("//span[@class='slds-icon_container slds-icon-utility-down']//span")).click();
	Thread.sleep(3000);
	
	}
	
	@Then ("Click on Delete icon") 
	public void ClickDeleteicon () throws InterruptedException {
	driver.findElement(By.xpath("//a[@title='Delete']")).click();
	Thread.sleep(3000);
	}
	
	@And ("Select Delete")
	public void selectDelete () {
	driver.findElement(By.xpath("//span[text()='Delete']")).click();
	
	}
	
	@Then ("VerifyText in searchbar {string}")
	public void VerifyText (String EnterName) throws InterruptedException {
	WebElement verifyText = driver.findElement(By.xpath("//input[@placeholder='Search this list...']"));
	verifyText.sendKeys(EnterName);
	verifyText.sendKeys(Keys.ENTER);
	Thread.sleep(3000);
	
	}
	
	@And ("DisplayNoMatches")
	public void NoMatches () {
	String nomatches = driver.findElement(By.xpath("//span[text()='No items to display.']")).getText();
	System.out.println(nomatches);
    
}
	
	}
