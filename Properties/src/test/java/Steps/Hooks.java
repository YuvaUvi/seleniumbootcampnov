package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Hooks extends BaseClass{

	
	@Before
	public void PreCondition() throws InterruptedException {
	WebDriverManager.chromedriver().setup();
	
	//Handle Notification
	ChromeOptions options=new ChromeOptions();
	options.addArguments("--disable-notifications");
	  
	driver  = new ChromeDriver(options);
	driver.manage().window().maximize();
	driver.get("https://login.salesforce.com");
	
	 //Login Page
	 driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
     driver.findElement(By.id("password")).sendKeys("SelBootcamp@1234");
     driver.findElement(By.id("Login")).click();
     Thread.sleep(10000);
	
	
	//toongle button
    driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
    Thread.sleep(3000);
   
    //Click on View all
    driver.findElement(By.xpath("//button[text()='View All']")).click();
    Thread.sleep(1000);
	
		
		
}

	@After
	public void PostCondition() {
	
    driver.close();
		
		
	}
	
}