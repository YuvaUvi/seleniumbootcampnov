package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Individual extends BaseClass{
	
	//3. Click Individuals from App Launcher
	@Given ("Click on Individual") 
	public void clickonIndividual() throws InterruptedException {
	WebElement click = driver.findElement(By.xpath("//p [ text () = 'Individuals']"));
	JavascriptExecutor exe = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click);
	Thread.sleep(2000);
	}
	
	//4. Click on the Dropdown icon in the Individuals tab
	@Then ("Click on the Dropdown icon in the Individuals tab")
	public void ClickOnDropdownInIndividualTab () throws InterruptedException {
	WebElement click1 = driver.findElement(By.xpath("//span [ text () = 'Individuals Menu']"));
	JavascriptExecutor exe1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click()", click1);
	Thread.sleep(2000);
	
	}
	
	 //5. Click on New Individual
	@And ("Click on New Individual")
	public void ClickOnNewIndividual () throws InterruptedException {
	WebElement click2 = driver.findElement(By.xpath("//span [text() = 'New Individual']"));
	JavascriptExecutor exe2 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click()", click2);
	Thread.sleep(3000);
	
	}
	
	//6. Enter the Last Name as 'Yathvik'
	@Then("Enter the Last Name  {string}")
	public void enter_the_last_name(String string) throws InterruptedException {
	driver.findElement(By.xpath("//input [@placeholder = 'Last Name']")).sendKeys(string);
	Thread.sleep(3000);
	}
	
	//7.Click save and verify Individuals Name
	@Then ("Click Save")
	public void Save () throws InterruptedException {
	driver.findElement(By.xpath("//button [ @title = 'Save']")).click();
	Thread.sleep(3000);
	  
	}

	 //8. Click Individuals from App Launcher
	@Given ("Click on Individual from App launcher")
	public void SelectIndividualfromAppLauncher () throws InterruptedException {
	WebElement click3 = driver.findElement(By.xpath("//p [ text () = 'Individuals']"));
	JavascriptExecutor exe = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click3);
	Thread.sleep(3000);
	}
	
	 // 9. Search the Individuals 'Yathvik'
	@And ("Search the Individuals {string}")
	public void SearchTheIndividuals (String SearchName) throws InterruptedException {
	driver.findElement(By.xpath("//input [@placeholder = 'Search this list...']")).
	sendKeys(SearchName + Keys.ENTER);
	Thread.sleep(3000);
	}
	
	 // 10. Click on the Dropdown icon and Select Edit
	@And ("Click on the Dropdown icon and Select Edit")
	public void SelectDropdownAndEdit () throws InterruptedException {
    WebElement click5 = driver.findElement(By.xpath("//a[contains(@class,'slds-button slds-button--icon-x-small')]"));
	JavascriptExecutor exe1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click5);
    driver.executeScript("arguments[0].click()",driver.findElement(By.xpath("//li[@class = 'uiMenuItem'][1]/a")));
    Thread.sleep(3000);
    
	}
	
	// 11.Select Salutation as 'Mr'
	@And ("Select Salutation as Mr")
	public void SelectSalutation () throws InterruptedException {
    WebElement click6 = driver.findElement(By.xpath("//a [@class = 'select']"));
	JavascriptExecutor exe3 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click()", click6);
	driver.findElement(By.xpath("//a [@title = 'Mr.']")).click();
	Thread.sleep(3000);
	}
	
    //  12.Enter the first name as 'Ganesh'
	@Then ("Enter the first name as {string}")
	public void EnterFirstName (String FirstName) throws InterruptedException {
	driver.findElement(By.xpath("//input [@placeholder = 'First Name']")).sendKeys(FirstName);
	Thread.sleep(3000);
	}
	
    //  13. Click on Save and 
	@And  ("Click on Save") 
	public void ClickOnSave () throws InterruptedException {
	driver.findElement(By.xpath("//button [ @title = 'Save']")).click();
	Thread.sleep(3000);
	}
	
	// 14. Verify the first name as 'Dev Yathvik'
	@Then ("Verify the name as Dev Yathvik")
	public void verifyTheName () throws InterruptedException {
	String text = driver.findElement(By.linkText("Dev Yathvik")).getText();
	System.out.println(text);
	Thread.sleep(3000);
	}
	 
	//15. Click Individuals from App Launcher
	@Given ("Click on Individual from App")
	public void ClickOnIndividualFromAPP () throws InterruptedException {
	WebElement click8 = driver.findElement(By.xpath("//p [ text () = 'Individuals']"));
	JavascriptExecutor exe = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click8);
	Thread.sleep(3000);
	}
	
	//16. Search the Individuals 'Yathvik'
	@And ("Search Individuals {string}")
	public void SearchIndividual (String Search) throws InterruptedException {
	driver.findElement(By.xpath("//input [@placeholder = 'Search this list...']")).
	sendKeys(Search+ Keys.ENTER);
	Thread.sleep(3000);
	 
	}
	//17. Click on the Dropdown icon and Select Delete
	@And ("Click on the Dropdown icon and Select Delete")
	public void ClickDropdownIconAndSelectDelete () throws InterruptedException {
	WebElement click7 = driver.findElement(By.xpath("//a[contains(@class,'slds-button slds-button--icon-x-small')]"));
	JavascriptExecutor exe1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click7);
	driver.executeScript("arguments [0].click()", driver.findElement(By.xpath("//a[@title = 'Delete']")));
	Thread.sleep(3000);
	}
	  
	

	//18.Click on the Delete option in the displayed popup window.
	@Given("Click on the Delete option in the displayed popup window.")
	public void click_on_the_delete_option_in_the_displayed_popup_window() throws InterruptedException {
	driver.executeScript("arguments [0].click()", driver.findElement(By.xpath("//button[@title = 'Delete']")));
	Thread.sleep(3000);
	
	}
	
	  
	//19. Verify Whether Individual is Deleted using Individual last name
	@Then ("Verify Whether Individual is Deleted using Individual last name")
	public void VerifyWhetherIndividualIsDeletedUsingLastName () {
	String text = driver.findElement(By.xpath("//span[text() = 'No items to display.']")).getText();
	System.out.println(text);
	  
	
	  
	  
}
	
	 
}  
	  

	


