Feature: Create Individual

#Background: 
#Given Launch the chrome browser
#And Load the application url


Scenario Outline: TC004_Create individual
Given Click on Individual
Then Click on the Dropdown icon in the Individuals tab
And Click on New Individual
And Enter the Last Name  <LastName>
Then Click Save 

Examples:
|LastName|
|'Yathvik'|

Scenario Outline: TC005_Edit Individual
Given Click on Individual from App launcher
And Search the Individuals <SearchName>
And Click on the Dropdown icon and Select Edit
And Select Salutation as Mr
Then Enter the first name as <FirstName>
And  Click on Save 
Then Verify the name as Dev Yathvik


Examples:
|SearchName|FirstName|
|'Yathvik'|'Dev'|

Scenario Outline: TC006_Delete Individual
Given Click on Individual from App 
And Search Individuals <Search>
And Click on the Dropdown icon and Select Delete
Given Click on the Delete option in the displayed popup window.
Then Verify Whether Individual is Deleted using Individual last name


Examples:
|Search|
|'Yathvik'|