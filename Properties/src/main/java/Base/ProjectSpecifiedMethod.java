package Base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ProjectSpecifiedMethod {

	public static ChromeDriver driver;
//	public String excelFileName;
	public static Properties prop;
	
	    @SuppressWarnings("deprecation")
	    @BeforeMethod
	    public void launchApplication() throws IOException {
	    	
	    	
	    //Steps to load the properties file
	    FileInputStream YS = new FileInputStream ("./src/main/java/utils/Env.properties");
	    prop = new Properties();
	    prop.load(YS);
	    
		WebDriverManager.chromedriver().setup();
	    
		 //Handle Notification
		  ChromeOptions options=new ChromeOptions();
		  options.addArguments("--disable-notifications");
		  
		   driver  = new ChromeDriver(options);
					    
		 
				
		  //Application url
		  driver.get("https://login.salesforce.com");
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    }
	
	//   @AfterMethod
	//   public void closebrowser() {
	//   driver.close();
	

  //  @DataProvider (name = "fetchData")
//	public Object [][] fetchData ()throws IOException, Exception{
  //  return ReadExcel.readExcelData(excelFileName);
   
    } 

