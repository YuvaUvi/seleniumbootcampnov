package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class OpportunityPage extends ProjectSpecifiedMethod{

	    public OpportunityPage clickNew() throws InterruptedException {
		Thread.sleep(1000);
        driver.findElement(By.xpath(prop.getProperty("OpportunityPage.clickNew.xpath"))).click();
        Thread.sleep(1000);
        return this;
        }
	
        public OpportunityPage enterSalesforce() throws InterruptedException {
 	    driver.findElement(By.xpath(prop.getProperty("OpportunityPage.enterSalesforce.xpath"))).sendKeys(prop.getProperty("enterSalesforce"));
 	   Thread.sleep(1000);
 	    return this;
 	    
        }
        
        public OpportunityPage enterDate() throws InterruptedException {
 	    driver.findElement(By.xpath(prop.getProperty("OpportunityPage.enterDate.xpath"))).sendKeys(prop.getProperty("enterDate"));
 	   Thread.sleep(1000);
 	    return this;
 	    
        }
       
       public OpportunityPage clickdropdown() throws InterruptedException {
       WebElement none = driver.findElement(By.xpath(prop.getProperty("OpportunityPage.clickdropdown.xpath")));
 	   JavascriptExecutor executor1 = (JavascriptExecutor) driver;
       driver.executeScript("arguments[0].click();", none);
       Thread.sleep(1000);
       return this;
       }
       
       public OpportunityPage clickAnalysis() throws InterruptedException {
 	   driver.findElement(By.xpath(prop.getProperty("OpportunityPage.clickAnalysis.xpath"))).click();
 	  Thread.sleep(1000);
 	   return this;
       }
       
       public OpportunityPage clickSave() {
 	   driver.findElement(By.xpath(prop.getProperty("OpportunityPage.clickSave.xpath"))).click();
 	   System.out.println("Salesforce Automation by Yuvaraj S");
 	   
 	   return this;
       }  
 	   
}

