package Pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.ProjectSpecifiedMethod;

public class NewTaskPage extends ProjectSpecifiedMethod{

	
	   public NewTaskPage ClickNewTask () throws InterruptedException {
	   driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("NewTaskPage.ClickNewTask.xpath"))));
	   Thread.sleep(5000);

	   return this;
	   }
	   public NewTaskPage EnterBootcamp () throws InterruptedException {
	   driver.findElement(By.xpath(prop.getProperty("NewTaskPage.EnterBootcamp.xpath"))).sendKeys(prop.getProperty("EnterBootcamp"));
	   Thread.sleep(1000);
	   return this;
	   }
	   public NewTaskPage SelectWaitingonsomeoneelse () throws InterruptedException {
	   driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("NewTaskPage.SelectWaitingonsomeone.xpath"))));
	   driver.executeScript("arguments [0].click()",driver.findElement(By.xpath(prop.getProperty("NewTaskPage.SelectWaitingonsomeoneelse.xpath"))));
	   Thread.sleep(1000);
		return this;
	   }
	   public NewTaskPage SelectDropDown () throws InterruptedException {
	   driver.findElement(By.xpath(prop.getProperty("NewTaskPage.SelectDropClick.xpath"))).click();
	   Thread.sleep(1000);
	   driver.findElement(By.xpath(prop.getProperty("NewTaskPage.SelectDropDown.xpath"))).click();
	   Thread.sleep(1000);
	   return this;
	   }

	   public NewTaskPage VerifyTaskCreatedMessage () {
	   driver.findElement(By.xpath(prop.getProperty("NewTaskPage.VerifyTaskCreatedMessage.xpath"))).click();
	    String text = driver.findElement(By.xpath(prop.getProperty("NewTaskPage.VerifyTaskMessage.xpath"))).getText();
        System.out.println(text);
	
        return this;
   }
}