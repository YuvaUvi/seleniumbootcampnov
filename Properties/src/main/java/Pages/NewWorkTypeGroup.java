package Pages;

import org.openqa.selenium.By;

import Base.ProjectSpecifiedMethod;

public class NewWorkTypeGroup extends ProjectSpecifiedMethod{

	 public NewWorkTypeGroup EnterSalesforceAutomationbyDev () throws InterruptedException {
		Thread.sleep(1000);
	  driver.findElement(By.xpath(prop.getProperty("NewWorkTypeGroup.EnterSalesforceAutomationbyDev.xpath"))).sendKeys(prop.getProperty("EnterSalesforceAutomationbyDev"));
	  Thread.sleep(1000);
	  return this;
	 }
	  

	 public NewWorkTypeGroup clickSave () throws InterruptedException {
	 driver.findElement(By.xpath(prop.getProperty("NewWorkTypeGroup.clickSave.xpath"))).click();
	 Thread.sleep(1000);
	 return this;
	 }
	 
	 public NewWorkTypeGroup clickVerify () {
	 String text = driver.findElement(By.xpath(prop.getProperty("NewWorkTypeGroup.clickVerify.xpath"))).getText();
	 System.out.println(text);
	
	 return this;
		
	}
	
}
