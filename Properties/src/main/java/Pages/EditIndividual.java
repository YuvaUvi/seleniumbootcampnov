package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class EditIndividual extends ProjectSpecifiedMethod{
	
	//Search the Individuals 'Yathvik'
	public EditIndividual SearchIndividualName () {
	driver.findElement(By.xpath(prop.getProperty("EditIndividual.SearchIndividualName.xpath"))).
	sendKeys("Yathvik"+ Keys.ENTER);
	
	return this;
		
	}

	//Click on the Dropdown icon a
	public EditIndividual ClickDropDown() {
	WebElement click1 = driver.findElement(By.xpath(prop.getProperty("EditIndividual.ClickDropDown.xpath")));
	JavascriptExecutor exe1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click();", click1);
	
	return this;
	}
	
	//Select Edit
	public EditIndividual SelectEdit() {
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("EditIndividual.SelectEdit.xpath"))));
	
	return this;
	
	}
	
	//Select Salutation 
	public EditIndividual SelectSalutation () {
	WebElement click3 = driver.findElement(By.xpath(prop.getProperty("EditIndividual.Salutation.xpath")));
	JavascriptExecutor exe3 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click()", click3);
		
	return this;	
		
	}
	
	//Select Salutation as MR
	public EditIndividual SalutationAsMR () {
	driver.findElement(By.xpath(prop.getProperty("EditIndividual.SalutationAsMR.xpath"))).click();
		
	return this;
	}
	
	//Enter the first name as 'Ganesh'
	public EditIndividual EnterFirstName () {
	driver.findElement(By.xpath(prop.getProperty("EditIndividual.EnterFirstName.xpath"))).sendKeys("Dev");
	
	return this;
			
	}
	
	//Click on Save 
	public EditIndividual ClickOnSave () {
	driver.findElement(By.xpath(prop.getProperty("EditIndividual.ClickOnSave.xpath"))).click();
	
	return this;
		
	}
	
	//Verify the first name as 'Ganesh'
	public EditIndividual VerifyFirstName () {
	String text = driver.findElement(By.linkText(prop.getProperty("EditIndividual.VerifyFirstName.linkText"))).getText();
    System.out.println(text);

    return this;
	}
	
	
}
