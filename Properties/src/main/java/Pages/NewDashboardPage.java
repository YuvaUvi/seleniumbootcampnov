package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;
import io.github.bonigarcia.wdm.webdriver.WebDriverBrowser;

public class NewDashboardPage extends ProjectSpecifiedMethod{

	
	
	// Enter the Dashboard name as "YourName_Workout"
		public NewDashboardPage enterName () throws InterruptedException {
		Thread.sleep(1000);
		WebElement frame = driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.enterName.xpath")));
		driver.switchTo().frame(frame);
		driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.enterNewDashboardName.xpath"))).sendKeys(prop.getProperty("NewDashboardName"));
		Thread.sleep(1000);		
		return this;
		}
				
		//Enter Description as Testing and Click on Create
		public NewDashboardPage enterDiscriptionName () throws InterruptedException {
		driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.enterDiscriptionName.xpath"))).sendKeys(prop.getProperty("Description"));
		Thread.sleep(1000);
		return this;
		}
			
		//Click on create
		public NewDashboardPage clickCreate () throws InterruptedException {
		driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.clickCreate.xpath"))).click();
		driver.switchTo().defaultContent();
		String title = driver.getTitle();
		System.out.println(title);
		Thread.sleep(1000);
		return this;

		}
		//click on done
		public NewDashboardPage clickDone () throws InterruptedException {
		WebElement frame = driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.clickDone.xpath")));
		driver.switchTo().frame(frame);
		Thread.sleep(3000);
		driver.findElement(By.xpath(prop.getProperty("NewDashboardPage.clickOnDone.xpath"))).click();
		Thread.sleep(5000);
	
		return this;
		
}
		//Verify the Dashboard is Created
		public NewDashboardPage verifyText () throws InterruptedException {
		String title = driver.getTitle();
		System.out.println(title);
		Thread.sleep(2000);
		
        
		return this;
}
        //Click on Subscribe
		public EditSubscription clicksubscribe () throws InterruptedException {
		driver.findElement(By.xpath(prop.getProperty("EditSubscription.clicksubscribe.xpath"))).click();
		Thread.sleep(2000);
		
		
		return new EditSubscription();
		
		}
		

}