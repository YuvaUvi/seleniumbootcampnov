package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class LegalPage extends ProjectSpecifiedMethod{

	    public LegalPage clickdropdown() throws InterruptedException {
	    Thread.sleep(1000);
	    WebElement dropdown = driver.findElement(By.xpath(prop.getProperty("LegalPage.clickdropdown.xpath")));
	    JavascriptExecutor executor1 = (JavascriptExecutor) driver;
	    driver.executeScript("arguments[0].click();", dropdown);
	    Thread.sleep(1000);
	    
	    return this;
	    }
	    public LegalPage clickNewlegalEntity() throws InterruptedException {
	    WebElement select = driver.findElement(By.xpath(prop.getProperty("LegalPage.clickNewlegalEntity.xpath")));
	    JavascriptExecutor executor2 = (JavascriptExecutor) driver;
	    driver.executeScript("arguments[0].click();", select);
	    Thread.sleep(1000);
	    
	    return this;
	    }
	    
	    public LegalPage EnterYourName () throws InterruptedException {
	    driver.findElement(By.xpath(prop.getProperty("LegalPage.EnterYourName.xpath"))).sendKeys(prop.getProperty("FirstName"));
	    Thread.sleep(1000);
	    return this;
	    }
	    
	    public LegalPage clickSave () throws InterruptedException {
	    driver.findElement(By.xpath(prop.getProperty("LegalPage.clickSave.xpath"))).click();
	    Thread.sleep(1000);
	    
	    return this;
	    }
	    
	    public LegalPage verify() {
	    String text = driver.findElement(By.xpath(prop.getProperty("LegalPage.verify.xpath"))).getText();
	    System.out.println(text);

	    return this;
}
}