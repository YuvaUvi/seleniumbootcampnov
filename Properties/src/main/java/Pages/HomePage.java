package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class HomePage extends ProjectSpecifiedMethod{

		public HomePage clicktongglebutton() throws InterruptedException {
			Thread.sleep(1000);
			 driver.findElement(By.xpath(prop.getProperty("HomePage.clicktongglebutton.xpath"))).click();
		
			 return this;
			
		}
		
		public HomePage clickviewall() throws InterruptedException {
			Thread.sleep(1000);
			driver.findElement(By.xpath(prop.getProperty("HomePage.clickviewall.xpath"))).click();
			
			return this;
		}
		public IndividualPage clickIndividual() throws InterruptedException {
		Thread.sleep(5000);
		WebElement click = driver.findElement(By.xpath(prop.getProperty("IndividualPage.clickIndividual.xpath")));
		JavascriptExecutor exe = (JavascriptExecutor) driver;
		driver.executeScript("arguments [0].click();", click);
		
		return new IndividualPage();

		}
		
		public LegalPage clickLegalEntities() throws InterruptedException {
		Thread.sleep(1000);
	    WebElement click1 = driver.findElement(By.xpath(prop.getProperty("LegalPage.clickLegalEntities.xpath")));
	    JavascriptExecutor executor = (JavascriptExecutor) driver;
	    driver.executeScript("arguments[0].click();", click1);
	    
	    
	    return new LegalPage();
	    
		}
		
        public HomePage clickSales () throws InterruptedException {
        Thread.sleep(1000);
		driver.findElement(By.xpath(prop.getProperty("HomePage.clickSales.xpath"))).click();
        return new HomePage();
}

        public OpportunityPage clickOpportunities() throws InterruptedException {
        Thread.sleep(1000);
        WebElement click2 = driver.findElement(By.xpath(prop.getProperty("OpportunityPage.clickOpportunities.xpath")));
 	    JavascriptExecutor executor = (JavascriptExecutor) driver;
        driver.executeScript("arguments[0].click();", click2);
        
        return new OpportunityPage();
        }

        public NewTaskPage clickNewTaskdropdown () throws InterruptedException {
        Thread.sleep(1000);
	    WebElement click3 = driver.findElement(By.xpath(prop.getProperty("NewTaskPage.clickNewTaskdropdown.xpath")));
	    JavascriptExecutor executor = (JavascriptExecutor) driver;
		driver.executeScript("arguments[0].click();", click3);
		
		return new NewTaskPage();
}

         public HomePage clickWorkTypeGroup () throws InterruptedException {
         Thread.sleep(1000);
		 WebElement click7 = driver.findElement(By.xpath(prop.getProperty("HomePage.clickWorkTypeGroup.xpath")));
		 JavascriptExecutor executor = (JavascriptExecutor) driver;
		 driver.executeScript("arguments[0].click();", click7);
		 Thread.sleep(10000);
		 return this;
		 
         }
         
		 public HomePage clickDropdownIconintheWorkTypeGroupstab () throws InterruptedException {
		 driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("HomePage.clickDropdownIconintheWorkTypeGroupstab.xpath"))));
		 Thread.sleep(5000);
		 
		 return this;
		 
		 }	  
		 public NewWorkTypeGroup clickNewWorkTypeGroup() throws InterruptedException {
		 driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("NewWorkTypeGroup.clickNewWorkTypeGroup.xpath"))));
		 Thread.sleep(3000);
		 
		 return new NewWorkTypeGroup ();
		 }
		 
		 public HomePage clickSalesConsole()  {
		 driver.findElement(By.xpath(prop.getProperty("HomePage.clickSalesConsole.xpath"))).click();
		 return this;
}

		 public HomePage clickHomeDropDown() throws InterruptedException  {
		 WebElement click4 = driver.findElement(By.xpath(prop.getProperty("HomePage.clickHomeDropDown.xpath")));
		 JavascriptExecutor executor4 = (JavascriptExecutor) driver;
		 driver.executeScript("arguments [0].click();", click4);
		 Thread.sleep(1000);
		 return this;
		 }
		 
      	//Select Home from the DropDown
		 public DashboardsPage clickDashboard () throws InterruptedException {
		 WebElement click5 = driver.findElement(By.xpath(prop.getProperty("DashboardsPage.clickDashboard.xpath")));
		 JavascriptExecutor exe5 = (JavascriptExecutor) driver;
		 driver.executeScript("arguments[0].click();", click5);
		 Thread.sleep(3000);
		 return new DashboardsPage();
		 }
		 
		 //Click Dashboards from App Launcher
		 public DashboardsPage SelectDashboardsFromAPPLauncher () throws InterruptedException {
		 WebElement click = driver.findElement(By.xpath(prop.getProperty("DashboardsPage.SelectDashboardsFromAPPLauncher.xpath")));
		 JavascriptExecutor executor = (JavascriptExecutor) driver;
		 driver.executeScript("arguments[0].click();", click);
		 Thread.sleep(1000);
		 
		 return new DashboardsPage();
		 
		 }
		 
		 //Click Work Type Groups from App Launcher
		 public WorkTypeGroupPage ClickWorkTypeGroupFromAppLauncher () {
		 WebElement click = driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.ClickWorkTypeGroupFromAppLauncher.xpath")));
		 JavascriptExecutor executor = (JavascriptExecutor) driver;
		 driver.executeScript("arguments[0].click();", click);
		 
		 return new WorkTypeGroupPage();
		 
}
		//Click Dashboards from App Launcher
		 public DeleteDashboard SelectDashboards () throws InterruptedException {
		 WebElement click = driver.findElement(By.xpath(prop.getProperty("DashboardsPage.SelectDashboards.xpath")));
		 JavascriptExecutor executor = (JavascriptExecutor) driver;
		 driver.executeScript("arguments[0].click();", click);
		 Thread.sleep(1000);
		 
		 return new DeleteDashboard();
	 
		 }
		
		 //Click Individuals from App Launcher
		 public EditIndividual ClickIndividual () {
		 WebElement click = driver.findElement(By.xpath(prop.getProperty("EditIndividual.ClickIndividual.xpath")));
		 JavascriptExecutor exe = (JavascriptExecutor) driver;
		 driver.executeScript("arguments [0].click();", click);
		 
		 return new EditIndividual ();
}	 
		 
}
