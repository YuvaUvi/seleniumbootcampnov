package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class EditSubscription extends ProjectSpecifiedMethod{

	//Select Frequency as "Daily"
	public EditSubscription clickDaily () throws InterruptedException {
	Thread.sleep(1000);
	driver.switchTo().defaultContent();
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickDaily.xpath"))).click();
	
	return this;
}

	//Time as 10:00 AM
	public EditSubscription selectTime () throws InterruptedException {
	Thread.sleep(1000);
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.selectTime.xpath"))).click();
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.selectDropDownClickTime.xpath"))).click();
	Thread.sleep(1000);
	return this;
}	
	//Click on Save
	public EditSubscription clickSave () throws InterruptedException {
	Thread.sleep(1000);
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickSave.xpath"))).click();
	
	return this;
	}

	//Verify "You started Dashboard Subscription" message displayed or not
	public EditSubscription VerifyMessage () throws InterruptedException {
	Thread.sleep(1000);
	String text = driver.findElement(By.xpath(prop.getProperty("EditSubscription.VerifyMessage.xpath"))).getText();
	System.out.println(text);
	Thread.sleep(1000);
	return this;
	
	}
	
	//Close the "YourName_Workout" tab
	public EditSubscription clickCloseYuvaraj () throws InterruptedException {
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickCloseYuvaraj.xpath"))).click();
	Thread.sleep(1000);
	return this;
	}
	
	//Click on Private Dashboards
	public EditSubscription clickPrivate () throws InterruptedException {
	WebElement Private = driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickPrivate.xpath")));
    JavascriptExecutor exe2 = (JavascriptExecutor) driver;
    driver.executeScript("arguments[0].click()",Private);
    Thread.sleep(1000);
    return this;
	}

	//Verify the newly created Dashboard available
	public EditSubscription SearchYourName () throws InterruptedException {
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.SearchYourName.xpath"))).sendKeys("Yuvaraj_Workout"+ Keys.ENTER);
    Thread.sleep(1000);
	return this;
	}
	
	//Click on dropdown for the item
	public EditSubscription clickEditDropDown () throws InterruptedException {
	WebElement dropdown = driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickEditDropDown.xpath")));
	JavascriptExecutor exe3 = (JavascriptExecutor) driver;
	driver.executeScript("arguments[0].click()",dropdown);
	Thread.sleep(1000);
	return this;
	
	}
	
	//Select Delete
	public EditSubscription clickDelete () throws InterruptedException {
	WebElement delete = driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickDelete.xpath")));
	JavascriptExecutor exe5 = (JavascriptExecutor) driver;
	driver.executeScript("arguments[0].click()", delete);
	Thread.sleep(1000);
	return this;
	
	}
	
	//Confirm the Delete
	public EditSubscription clickConfirmDelete () throws InterruptedException {
	driver.findElement(By.xpath(prop.getProperty("EditSubscription.clickConfirmDelete.xpath"))).click();
	Thread.sleep(1000);
	
	return this;
	}
	
	//Verify the item is not available under Private Dashboard folder
	public EditSubscription VerifyDelete () {
	String text = driver.findElement(By.xpath(prop.getProperty("EditSubscription.VerifyDelete.xpath"))).getText();
	System.out.println(text);
	
	return this;
	}
}