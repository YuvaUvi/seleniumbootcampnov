package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Base.ProjectSpecifiedMethod;

public class WorkTypeGroupPage extends ProjectSpecifiedMethod{
	
	
	//Search the Work Type Group 'Salesforce Automation by Your Name'
	public WorkTypeGroupPage SearchWorkTypeGroupByYourName () throws InterruptedException {
	Thread.sleep(1000);
	driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.SearchWorkTypeGroupByYourName.xpath"))).
    sendKeys("Salesforce Automation by Yuvaraj S"+ Keys.ENTER);
	
	return this;	
	}

	//Click on the Dropdown icon 
	public WorkTypeGroupPage ClickDropDown () throws InterruptedException {
	Thread.sleep(1000);
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.ClickDropDown.xpath"))));
	
	return this;
	}
	
	//Select Edit
	public WorkTypeGroupPage ClickEdit () throws InterruptedException {
	Thread.sleep(1000);
	driver.executeScript("arguments[0].click()", driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.ClickEdit.xpath"))));
	
	return this;
	}
	
	//Enter Description as 'Automation'
	public WorkTypeGroupPage EnterDiscription() throws InterruptedException {
	Thread.sleep(1000);
	driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.EnterDiscription.xpath"))).sendKeys("Automation");
	
	return this;
	}
	
	//Select 
	public WorkTypeGroupPage select () throws InterruptedException {
	Thread.sleep(1000);
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.select.xpath"))));
	
	return this;
	}
	
	//Select Group Type as 'Capacity'
	public WorkTypeGroupPage GroupTypeAsCapacity () throws InterruptedException {
	Thread.sleep(1000);
	driver.executeScript("arguments[0].click()",driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.GroupTypeAsCapacity.xpath"))));
	
	return this;
		
	}
	
	//Click on Save
	public WorkTypeGroupPage ClickOnSave () throws InterruptedException {
	Thread.sleep(1000);
	driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.ClickOnSave.xpath"))).click();
	
	return this;
	
	}
	
	//Click on 'Salesforce Automation by Your Name'
	public WorkTypeGroupPage ClickOnSalesforceAutomation () throws InterruptedException {
	Thread.sleep(1000);
	WebElement click1 = driver.findElement(By.xpath(prop.getProperty("WorkTypeGroupPage.ClickOnSalesforceAutomation.xpath")));
	JavascriptExecutor executor1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments[0].click();", click1);
		
	return this;
	}
	
	//Verify Description as 'Automation'
	public WorkTypeGroupPage VerifyDescriptionAutomation () throws InterruptedException {
	Thread.sleep(1000);
	String text = driver.findElement(By.xpath("WorkTypeGroupPage.VerifyDescriptionAutomation.xpath")).getText();
	Thread.sleep(1000);
	System.out.println(text);
	
	return this;
		
	}
	
	
	
}
