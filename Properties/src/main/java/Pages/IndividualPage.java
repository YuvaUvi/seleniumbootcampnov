package Pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Base.ProjectSpecifiedMethod;

public class IndividualPage extends ProjectSpecifiedMethod{
	

	public IndividualPage clickIndividualMenu() throws InterruptedException {
	Thread.sleep(3000);
	WebElement click1 = driver.findElement(By.xpath(prop.getProperty("IndividualPage.clickIndividualMenu.xpath")));
	JavascriptExecutor exe1 = (JavascriptExecutor) driver;
	driver.executeScript("arguments [0].click()", click1);
	Thread.sleep(1000);
	  
	 return this;
	}
	 public IndividualPage clickNewIndividual() throws InterruptedException {
	  WebElement click2 = driver.findElement(By.xpath(prop.getProperty("IndividualPage.clickNewIndividual.xpath")));
	  JavascriptExecutor exe2 = (JavascriptExecutor) driver;
	  driver.executeScript("arguments [0].click()", click2);
	  Thread.sleep(1000);
	  
	  return this;
	 }
	  
	 public IndividualPage enterYathvik() throws InterruptedException {
	 driver.findElement(By.xpath(prop.getProperty("IndividualPage.enterYathvik.xpath"))).sendKeys(prop.getProperty("LastName"));
	 Thread.sleep(1000);
	 return this;
	 }
	  
	 public IndividualPage clicksave() throws InterruptedException {
	 driver.findElement(By.xpath(prop.getProperty("IndividualPage.clicksave.xpath"))).click();
	 Thread.sleep(1000);
	 
	 return this;
	 }
	 
	 
}
