package Pages;



import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import Base.ProjectSpecifiedMethod;

public class LoginPage extends ProjectSpecifiedMethod{
	

	public LoginPage EnterUserName () throws InterruptedException {
	driver.findElement(By.id(prop.getProperty("LoginPage.username.Id"))).sendKeys(prop.getProperty("username"));
	return this;
	}
    public LoginPage EnterPassword () {
    driver.findElement(By.id(prop.getProperty("LoginPage.password.Id"))).sendKeys(prop.getProperty("password"));
    	
    	return this;
}
    
    public HomePage clickLoginButton() {
    driver.findElement(By.id(prop.getProperty("Loginpage.LoginButton.Id"))).click();	
    
    
    return new HomePage();
    
    }
    
    
    
    
    }
