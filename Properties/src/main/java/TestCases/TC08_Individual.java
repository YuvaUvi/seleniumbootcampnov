package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC08_Individual extends ProjectSpecifiedMethod{

	@Test
	public void runIndividual () throws InterruptedException {
		
		 new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickIndividual()
    	.clickIndividualMenu()
		.clickNewIndividual()
		.enterYathvik()
		.clicksave();
	
	}
	
}
