package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC11_EditIndividual extends ProjectSpecifiedMethod{
	
	@Test
	public void runTC11_EditIndividual () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.ClickIndividual()
		.SearchIndividualName()
		.ClickDropDown()
		.SelectEdit()
		.SelectSalutation()
		.SalutationAsMR()
		.EnterFirstName()
		.ClickOnSave()
		.VerifyFirstName();
	}
		
	

}
