package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC05_VerifyDashboardSubscribe extends ProjectSpecifiedMethod{


		@Test
		public void runVerifyDashboardSubscribe () throws InterruptedException {
			
			new LoginPage()
			.EnterUserName()
			.EnterPassword()
			.clickLoginButton()
			.clicktongglebutton()
			.clickviewall()
			.SelectDashboardsFromAPPLauncher()
			.SearchRecentDashboards()
			.clickDropDown()
			.SelectSubscribe()
			.SeleckDaily()
			.ClickSave()
			.VerifySubscribe();
	}

}
