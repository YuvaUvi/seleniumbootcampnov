package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC10_DeleteDashboard extends ProjectSpecifiedMethod{

	
	@Test
	public void runTC10_DeleteDashboard () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.SelectDashboards()
		.SearchByName()
		.ClickDropDownIcon()
		.DeleteOptionPopUP()
		.ClickOnDeleteOption()
		.VerifyDeletedText();
		
		
		
	}
}
