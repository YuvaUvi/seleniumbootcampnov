package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC02_CreateTask extends ProjectSpecifiedMethod{

    @Test
	public void runCreateTask () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickSales()
		.clickNewTaskdropdown()
		.ClickNewTask()
		.EnterBootcamp()
		.SelectWaitingonsomeoneelse()
		.SelectDropDown()
		.VerifyTaskCreatedMessage();
		
		
		
	}
	
	
}
