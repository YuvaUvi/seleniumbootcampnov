package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC09_EditWorkTypeGroup extends ProjectSpecifiedMethod{

	@Test
	public void runEditWorkTypeGroup () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.ClickWorkTypeGroupFromAppLauncher()
		.SearchWorkTypeGroupByYourName()
		.ClickDropDown()
		.ClickEdit()
		.EnterDiscription()
		.select()
		.GroupTypeAsCapacity()
		.ClickOnSave()
		.ClickOnSalesforceAutomation()
		.VerifyDescriptionAutomation();
	}
}
