package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC04_Opportunity extends ProjectSpecifiedMethod{

//	@BeforeTest
//	public void setfile () {
//		excelFileName = "Opportunity";
	//(dataProvider = "fetchData")
	@Test
	public void runOpportunity () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickSales()
		.clickOpportunities()
		.clickNew()
		.enterSalesforce()
		.enterDate()
		.clickdropdown()
		.clickAnalysis()
		.clickSave();
	}
}
