package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC03_Dashboards extends ProjectSpecifiedMethod{

	@Test
	public void runDashboards () throws InterruptedException {
 
		 new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickSalesConsole()
		.clickHomeDropDown()
		.clickDashboard()
		.clickNewDashboard()
		.enterName()
		.enterDiscriptionName()
		.clickCreate()
		.clickDone()
		.verifyText()
		.clicksubscribe()
		.clickDaily()
		.selectTime()
		.clickSave()
		.VerifyMessage()
		.clickCloseYuvaraj()
		.clickPrivate()
		.SearchYourName()
		.clickEditDropDown()
		.clickDelete()
		.clickConfirmDelete()
		.VerifyDelete();
		
		
		

	}
}