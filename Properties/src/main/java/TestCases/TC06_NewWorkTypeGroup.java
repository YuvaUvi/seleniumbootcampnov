package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC06_NewWorkTypeGroup extends ProjectSpecifiedMethod {

	@Test
	public void runNewWorkTypeGroup () throws InterruptedException {
		
		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickWorkTypeGroup()
		.clickDropdownIconintheWorkTypeGroupstab()
		.clickNewWorkTypeGroup()
		.EnterSalesforceAutomationbyDev()
		.clickSave()
		.clickVerify();
			
		
	}
}
