package TestCases;

import org.testng.annotations.Test;

import Base.ProjectSpecifiedMethod;
import Pages.LoginPage;

public class TC07_Legal extends ProjectSpecifiedMethod{

	@Test
	public void runLegal () throws InterruptedException {

		new LoginPage()
		.EnterUserName()
		.EnterPassword()
		.clickLoginButton()
		.clicktongglebutton()
		.clickviewall()
		.clickLegalEntities()
		.clickdropdown()
		.clickNewlegalEntity()
		.EnterYourName()
		.clickSave()
		.verify();
		
}
}